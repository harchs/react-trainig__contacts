import './App.css';
import React from 'react';
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
} from 'react-router-dom';

import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducer from './reducer';

import Contacts from './containers/contacts';
import Contact from './containers/contact';
import NotFound from './components/not-found';

import { getContacts } from './actions/contacts';

const store = createStore(reducer, compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
));
store.dispatch(getContacts());

const App = () => (
  <Provider store={store}>
    <Router>
      <div>
        <Switch>
          <Route exact path="/" component={Contacts} />
          <Route exact path="/contacts" component={Contacts} />
          <Route exact path="/contacts/:id" component={Contact} />
          <Route component={NotFound}/>
        </Switch>
      </div>
    </Router>
  </Provider>
)

export default App;
