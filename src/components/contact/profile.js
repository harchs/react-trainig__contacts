import React from 'react';
import Avatar from '../../avatar.jpg';

const Profile = (props) => (
  <div className='profile'>
    <img src={Avatar}/>
    <div className='name'>
      <span className='first-name'>{props.firstName}</span>
      <span className='last-name'>{props.lastName}</span>
    </div>
  </div>
)

export default Profile;
