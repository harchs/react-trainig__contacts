import React, { Component } from 'react';

class Phone extends Component {
  state = {
    phoneNumber: '',
  }

  _phoneNumberFormat = (phoneNumber) => {
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3, 6)} ${phoneNumber.slice(6)}`
  }

  componentWillMount(_nextProps) {
    const phoneNumber = this.props.phoneNumber;
    this.setState({phoneNumber: this._phoneNumberFormat(phoneNumber)});
  }

  render(){
    return (
      <div className='detail'>
        <span className='label'>Mobile</span>
        <span className='value'>{this.state.phoneNumber}</span>
      </div>
    )
  }
}

export default Phone;
