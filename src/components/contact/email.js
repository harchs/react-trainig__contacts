import React from 'react';

const Email = (props) => (
  <div className='detail'>
    <span className='label'>Email</span>
    <span className='value'>{props.email}</span>
  </div>
)

export default Email;
