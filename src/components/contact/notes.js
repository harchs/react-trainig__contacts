import React from 'react';

const Notes = (props) => (
  <div className='detail'>
    <span className='label'>Notes</span>
    <span className='value'>{props.notes}</span>
  </div>
)

export default Notes;
