import React from 'react';
import PropTypes from 'prop-types';

import Avatar from '../../avatar.jpg';

const ItemList = ({contact}) => {
  return (
    <div id={contact.id} className='contact'>
      <a href={`/contacts/${contact.id}`}>
        <img src={Avatar}/>
        <span className='name'>{contact.firstName} {contact.lastName}</span>
      </a>
    </div>
  )
}

ItemList.propTypes = {
  contact: PropTypes.object
};

export default ItemList;
