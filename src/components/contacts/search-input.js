import React from 'react';

const Search = (props) => (
  <input type='text' className='search-input' onKeyUp={props.onFilterContacts}/>
)

export default Search;
