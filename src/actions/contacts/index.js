import Contacts from '../../contacts.json';

export const SET_CONTACTS = 'SET_CONTACTS';

const setContacts = contacts => {
  return {
    type: SET_CONTACTS,
    contacts,
  };
};

export function getContacts() {
  return dispatch => {
    dispatch(setContacts(Contacts));
  }
}

export function filterContacts(_filter) {
  return dispatch => {
    let filteredContacts = Object.assign([], Contacts);
    if (_filter){
      filteredContacts = filteredContacts.filter(
        (contact) =>
          contact.firstName.match(RegExp(_filter, 'i')) ||
          contact.lastName.match(RegExp(_filter, 'i'))
      );
    }
    dispatch(setContacts(filteredContacts));
  }
}
