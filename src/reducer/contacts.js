import { SET_CONTACTS } from '../actions/contacts';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CONTACTS:
      return action.contacts;
    default:
      return state;
  }
};
