import React, { Component } from 'react';

import ContactItemList from '../components/contacts/item-list'
import SearchInput from '../components/contacts/search-input'

import * as actions from '../actions/contacts';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

export class Contacts extends Component {

  handleFilterContact = (e) => {
    const _filter = e.target.value;
    this.props.actions.filterContacts(_filter);
  }

  render() {
    return (
      <div>
        <SearchInput onFilterContacts={this.handleFilterContact} />
        <div className='contacts list'>
          {this.props.contacts.map(contact => {
            return (
              <ContactItemList
                key={contact.id}
                contact={contact}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = _state => {
  const {contacts} = _state;
  return {
    contacts
  };
};

const mapDispatchToProps = _dispatch => {
  return {
    actions: bindActionCreators(actions, _dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
