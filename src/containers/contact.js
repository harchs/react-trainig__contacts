import React, { Component } from 'react';
import './../App.css';

import Profile from '../components/contact/profile';
import Phone from '../components/contact/phone';
import Email from '../components/contact/email';
import Notes from '../components/contact/notes';

class Contact extends Component {
  state = {
    contact: {}
  }

  componentWillMount(_nextProps) {
    const contactId = this.props.match.params.id;
    const contact = window.contacts.find((contact) => contact.id === contactId);
    this.setState({contact: contact});
  }

  render() {
    const {contact} = this.state;

    return (
      <div className="contact details">
        <Profile
          firstName={contact.firstName}
          lastName={contact.lastName}
        />
        <Phone
          phoneNumber={contact.phoneNumber}
        />
        <Email
          email={contact.email}
        />
      <Notes
          notes={contact.notes}
        />
      </div>
    );
  }
}

export default Contact;
